with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "node";
    buildInputs = [
        nodejs
		zola
    ];
    shellHook = ''
        export PATH="$PWD/node_modules/.bin/:$PATH"
		function build {
            rm -r content/documentation static/api
            cp -rv garage/doc/book content/documentation
            cp -rv garage/doc/api static/api
			npm install
			npx tailwindcss -i ./src/input.css -o ./static/style.css --minify
			zola build -u https://garagehq.deuxfleurs.fr
		}
    '';
}
