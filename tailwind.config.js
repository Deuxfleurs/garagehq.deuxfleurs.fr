module.exports = {
  content: [
    "./templates/**/*.html",
    "./static/js/site.js",
  ],
  theme: {
    extend: {
      colors: {
        'garage-orange': "#FF9329",
        'garage-gray': "#4E4E4E",
      },
      backgroundSize: {
        'size-200': '200% 200%',
      },
      backgroundPosition: {
          'pos-0': '0% 0%',
          'pos-100': '100% 100%',
      },
    },
  },
  variants: {
    width: ["responsive", "hover", "focus"],
  },
  plugins: [],
}
