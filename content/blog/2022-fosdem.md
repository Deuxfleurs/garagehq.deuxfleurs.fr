+++
title="Garage will be at FOSDEM'22"
date=2022-02-02
+++

*FOSDEM is an international meeting about Free Software, organized from Brussels.
On next Sunday, February 6th, 2022, we will be there to present Garage.*

<!-- more -->

---

In 2000, a Belgian free software activist going by the name of Raphael Baudin
set out to create a small event for free software developers in Brussels.
This event quickly became the "Free and Open Source Developers' European Meeting",
shorthand FOSDEM. 22 years later, FOSDEM is a major event for free software developers
around the world. And for this year, we have the immense pleasure of announcing
that the Deuxfleurs association will be there to present Garage.

The event is usually hosted by the Université Libre de Bruxelles (ULB) and welcomes
around 5000 people. But due to COVID, the event has been taking place online
in the last few years. Nothing too unfamiliar to us, as the organization is using
the same tools as we are: a combination of Jitsi and Matrix.

We are of course extremely honored that our presentation was accepted.
If technical details are your thing, we invite you to come and share this event with us.
In all cases, the event will be recorded and available as a VOD (Video On Demand) 
afterward. Concerning the details of the organization:

**When?** On Sunday, February 6th, 2022, from 10:30 AM to 11:00 AM CET.

**What for?** Introducing the Garage storage platform.

**By whom?** The presentation will be made by Alex,
    other developers will be present to answer questions.

**For who?** The presentation is targeted to a technical audience that is knowledgeable in software development or systems administration.

**Price:** FOSDEM'22 is an entirely free event.

**Where?** Online, in the Software Defined Storage devroom.

- [Join the room interactively (video and chat)](https://chat.fosdem.org/#/room/%23sds-devroom:fosdem.org)
- [Join the room as a spectator (video only)](https://live.fosdem.org/watch/dsds)
- [Event details on the FOSDEM'22 website](https://fosdem.org/2022/schedule/event/sds_garage_introduction)

And if you are not so much of a technical person, but you're dreaming of
a more ethical and emancipatory digital world,
keep in tune with news coming from the Deuxfleurs association
as we will likely have other events very soon!


